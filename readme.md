Chain: Doubly-linked-lists in C
===============================

Originally part of the [Vector](https://gitlab.com/gobusto/vector) project, now
split out due to being reused by [Conqore2](https://gitlab.com/gobusto/rbembed)
and various other projects.

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
licence, allowing you to use or modify it for any purpose, including commercial
and closed-source projects. All I ask in return is that _proper attribution_ is
given (i.e. don't remove my name from the copyright text in the source code and
perhaps include me on the "credits" screen, if your program has such a thing).
